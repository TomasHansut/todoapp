import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Input() public content;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  public modalForm: FormGroup;
  title: string;
  btn: string;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.createModal(this.content);
  }

  createModal(content: any) {
    if (content === 'delete') {
      this.title = 'DELETE TODO ?';
      this.btn = 'DELETE';
    } else if (content === 'complete') {
      this.title = 'COMPLETE TO DO ?';
      this.btn = 'COMPLETE';
    }
  }
  passBack() {
    this.passEntry.emit(this.content);
  }
}
