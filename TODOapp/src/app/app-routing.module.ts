import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const appRoutes: Routes = [
    { path: '', redirectTo: '/todolist', pathMatch: 'full' },
    { path: 'todoform', loadChildren: './todoform/todoform.module#TodoformModule'},
    { path: 'todolist',  loadChildren: './todolist/todolist.module#TodolistModule' },
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes , {
        preloadingStrategy: PreloadAllModules
      }),
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {

}
