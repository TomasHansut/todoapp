export interface Todo {
    id: number;
    title: string;
    text: string;
    createdAt: number;
    completedAt: number;
}
