import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { Todo } from './todo.model';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class DataStorageService {

  public postForm: Array<Todo>;
  public id: number;
  public editMode = false;

  constructor(private http: HttpClient) { }

  public addTodo(data: FormGroup) {
      this.postForm = data.value;
  }

  public Post() {
    return this.http.post(environment.apiUrl, this.postForm);
  }

  public Get() {
    return this.http.get<Array<Todo>>(environment.apiUrl);
  }

  public Complete(id: Todo[]) {
    return this.http.post(environment.apiUrl + '/complete/' + id, null);
  }

  public Delete(id: Todo[]) {
    return this.http.delete(environment.apiUrl  + '/' + id);
  }

  public EditID(id: Todo['id']) {
    this.id = id;
    this.editMode = true;
  }

  public Edit() {
    return this.http.put(environment.apiUrl + '/' + this.id, this.postForm);
  }
}
