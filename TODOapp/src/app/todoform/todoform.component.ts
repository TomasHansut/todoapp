import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { DataStorageService } from '../Shared/data-storage.service';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-todoform',
  templateUrl: './todoform.component.html',
  styleUrls: ['./todoform.component.scss']
})
export class TodoformComponent implements OnInit, OnDestroy {
  addToForm: FormGroup;
  private submitsubscription: Subscription;
  private editsubscription: Subscription;
  editMode: boolean;

  constructor(private dataStorage: DataStorageService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.addToForm = new FormGroup({
      title: new FormControl(null, Validators.required),
      text: new FormControl(null),
    });
    this.editMode = this.dataStorage.editMode;
  }

  Submit() {
    if (this.addToForm.valid) {
      this.dataStorage.addTodo(this.addToForm);
      this.submitsubscription = this.dataStorage.Post().subscribe((data) => {
        this.onCancel();
      });
    }
  }

  EditSubmit() {
    if (this.addToForm.valid) {
      this.dataStorage.addTodo(this.addToForm);
      this.editsubscription = this.dataStorage.Edit().subscribe((data) => {
        this.onCancel();
      });
    }
  }

  public onCancel(): void {
    if (this.editMode) {
        this.router.navigate(['../'], { relativeTo: this.route });
    } else {
        this.router.navigate(['../'], { relativeTo: this.route });
    }
}
  ngOnDestroy() {
    if (this.editsubscription) {
      this.editsubscription.unsubscribe();
    }
    if (this.submitsubscription) {
      this.submitsubscription.unsubscribe();
    }
    this.dataStorage.editMode = false;
  }
}
