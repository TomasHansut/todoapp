import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TodoformComponent } from './todoform.component';


const todoformRoutes: Routes = [
    { path: '', component: TodoformComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(todoformRoutes)
    ],
    exports: [RouterModule]
})
export class TodoformRoutingModule {}
