import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TodoformComponent } from './todoform.component';
import { TodoformRoutingModule } from './todoform-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    TodoformRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    TodoformComponent,
  ]
})
export class TodoformModule {

}
