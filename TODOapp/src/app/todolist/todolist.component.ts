import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

import { Todo } from '../Shared/todo.model';
import { DataStorageService } from '../Shared/data-storage.service';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.scss']
})
export class TodolistComponent implements OnInit, OnDestroy {
  public todos: Array<Todo>;
  private donesubscription: Subscription;
  private delsubscription: Subscription;
  private getsubscription: Subscription;
  private modalsubscription: Subscription;

  constructor(private dataStorage: DataStorageService, private modalService: NgbModal) { }

  ngOnInit() {
    this.Get();
  }

  Get() {
    this.getsubscription = this.dataStorage.Get().subscribe(
      (val) => {
          console.log('GET call successful value returned in body', val);
          this.todos = val;
      },
      response => {
          console.log('GET call in error', response);
      },
      () => {
          console.log('GET observable is now completed.');
    });
  }

  open(content: any, id: Todo[]) {
    const modalRef = this.modalService.open(ModalComponent);
    modalRef.componentInstance.content = content;
    this.modalsubscription = modalRef.componentInstance.passEntry.subscribe((receivedEntry) => {
      if (receivedEntry === 'complete') {
        this.Done(id);
      }
      if (receivedEntry === 'delete') {
        this.Del(id);
      }
    });
  }

  Done(id: Todo[]) {
    this.donesubscription = this.dataStorage.Complete(id).subscribe(
      (val) => {
        console.log('DONE call successful value returned in body', val);
      },
      response => {
        console.log('DONE call in error', response);
      },
      () => {
        console.log('DONE observable is now completed.');
        this.Get();
    });
  }

  Del(id: Todo[]) {
    this.delsubscription = this.dataStorage.Delete(id).subscribe(
      (val) => {
        console.log('DELETE call successful value returned in body', val);
      },
      response => {
        console.log('DELETE call in error', response);
      },
      () => {
        console.log('DELETE observable is now completed.');
        this.Get();
    });
  }

  Edit(id: Todo['id']) {
    this.dataStorage.EditID(id);
  }

  ngOnDestroy() {
    if (this.delsubscription) {
      this.delsubscription.unsubscribe();
    }
    if (this.donesubscription) {
      this.donesubscription.unsubscribe();
    }
    if (this.getsubscription) {
      this.getsubscription.unsubscribe();
    }
    if (this.modalsubscription) {
      this.modalsubscription.unsubscribe();
    }
  }
}
