import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TodolistComponent } from './todolist.component';

const todolistRoutes: Routes = [
    { path: '', component: TodolistComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(todolistRoutes)
    ],
    exports: [RouterModule]
})
export class TodolistRoutingModule {}
