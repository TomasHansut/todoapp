import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { TodolistComponent } from './todolist.component';
import { TodolistRoutingModule } from './todolist-routing.module';

@NgModule({
    imports: [
        CommonModule,
        TodolistRoutingModule,
        NgbModule,
      ],
      declarations: [
        TodolistComponent,
      ],
})
export class TodolistModule {}
